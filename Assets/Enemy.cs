﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
	bool m_Die;
	Animator m_Animator;
	Rigidbody m_rigidBody;
	MeshCollider m_meshCollidar;
	public static int life = 30;
	public GameObject gameObjectText;
	TextMesh lifeText;
	//public GameObject player;
	// Use this for initialization
	void Start () {
		lifeText = gameObjectText.GetComponent<TextMesh> ();
		m_Animator = GetComponent<Animator>();
		m_rigidBody = GetComponent<Rigidbody>();
		m_meshCollidar = GetComponent<MeshCollider>();
		lifeText.text = life+"";
	}
	
	// Update is called once per frame
	void Update () {
		if(life <= 0) {
			m_meshCollidar.enabled = false;
			Destroy (m_rigidBody);
			m_Die = true;
			Destroy (lifeText);
		}
		m_Animator.SetBool("Die", m_Die);
	}
	void OnCollisionEnter(Collision collision) {
		//print (collision.relativeVelocity);
		if(collision.gameObject.CompareTag("Sword")) {
			//gameObject.
			life = life - 10;
			lifeText.text = life +"";
			//m_meshCollidar.enabled = false;
			//Destroy (m_rigidBody);
			//m_Die = true;
		}
	}

	void  OnTriggerEnter(Collider col) {
		//GameObject player = col.gameObject;
	//	Animator animatorPlayer = player.GetComponent<Animator>();
	//	AnimatorStateInfo animStateInfo = animatorPlayer.GetCurrentAnimatorStateInfo (0);
		//int attack = 1080829965;
		//print (attack);
		//print (animStateInfo.nameHash);
		//print(animatorPlayer.GetBool("Attack"));
		//print(m_rigidBody.);
		if(col.gameObject.CompareTag("Sword")) {
			//gameObject.
		//	life = life - 10;
		//	lifeText.text = life +"";
			//m_meshCollidar.enabled = false;
			//Destroy (m_rigidBody);
			//+m_Die = true;
		}
		//Destroy(col.gameObject);
	}


	void OnTriggerStay(Collider other)
	{
		print ("tamamm");
		if(other.gameObject.CompareTag("Sword")) {
			//gameObject.
			life = life - 10;
			lifeText.text = life +"";
			//m_meshCollidar.enabled = false;
			//Destroy (m_rigidBody);
			//+m_Die = true;
		}
	}
}
