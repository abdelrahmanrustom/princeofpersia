﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
	public static Animator m_Animator;
	bool m_Attack;
	bool m_Run;
	bool m_Jump;
	bool m_Defend;
	bool m_Roll;
	bool m_Die;
	bool m_WallRun;
	public static int life = 100;
	public GameObject gameObjectText;
	TextMesh lifeText;
	public GameObject sword;
	private BoxCollider m_meshCollidarSword;
	float m_StationaryTurnSpeed = 180;
	float m_MovingTurnSpeed = 360;
	float m_ForwardAmount;
	private Vector3 m_CamForward;    
	private Vector3 m_Move;
	private Transform m_Cam; 
	float m_TurnAmount;
	float speed = 100f;
	// Use this for initialization
	void Start () {
		lifeText = gameObjectText.GetComponent<TextMesh> ();
		m_Animator = GetComponent<Animator>();
		m_meshCollidarSword = sword.GetComponent<BoxCollider>();
		//+lifeText = GetComponent<TextMesh> ();

	}
	
	// Update is called once per frame
	void Update () {
		lifeText.text = life+"";
		m_Attack = Input.GetMouseButtonDown(0);

		if(m_Attack) {
			m_meshCollidarSword.enabled = true;
		} else {
			m_meshCollidarSword.enabled = false;
		}

		m_Defend = Input.GetMouseButton(1);
		m_Run = Input.GetKey(KeyCode.LeftShift);
		m_Jump = Input.GetKeyDown(KeyCode.Space);
		m_Roll = Input.GetKeyDown(KeyCode.R);
		m_Die = Input.GetKeyDown(KeyCode.P);
		m_WallRun = Input.GetKeyDown(KeyCode.M);
		ApplyExtraTurnRotation ();
		m_Animator.SetBool("Attack", m_Attack);
		m_Animator.SetBool("Run", m_Run);
		m_Animator.SetBool ("Jump", m_Jump);
		m_Animator.SetBool ("Block", m_Defend);
		m_Animator.SetBool ("Roll", m_Roll);
		m_Animator.SetBool ("Die", m_Die);
		m_Animator.SetBool ("WallRun", m_WallRun);
	
	}

	void OnTriggerStay(Collider other)
	{
		//print ("tamamm");
		if(other.gameObject.CompareTag("EnemyHand")) {
			//gameObject.
			//life = life - 10;
			//lifeText.text = life +"";
			//m_meshCollidar.enabled = false;
			//Destroy (m_rigidBody);
			//+m_Die = true;
		} else {
			if(other.gameObject.CompareTag("Box") &&  Input.GetKeyDown(KeyCode.E) ) {
				other.gameObject.GetComponent<Animator> ().enabled = true;
				other.gameObject.GetComponent<BoxCollider> ().enabled = false;
				sword.active = true;
			}
		}
	}

	void ApplyExtraTurnRotation()
	{
		// help the character turn faster (this is in addition to root rotation in the animation)
		//float turnSpeed = Mathf.Lerp(m_StationaryTurnSpeed, m_MovingTurnSpeed, m_ForwardAmount);
		//transform.Rotate(0, m_TurnAmount * turnSpeed * Time.deltaTime, 0);
		transform.Rotate(0, Input.GetAxis("Horizontal")*Time.deltaTime*speed, 0);


		//transform.Rotate(180,180, 180);

	}

}
